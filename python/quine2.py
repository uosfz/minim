import itertools
from util import from_mint, to_mint, weight, cost, minterms_from_imp, covers

class Quine2Line:
    def __init__(self, imp):
        self.imp = imp
        self.data = (1 << len(imp)) * [False]
        for mint in map(to_mint, minterms_from_imp(imp)):
            self.data[mint] = True

    def covered_by(self, other):
        for l, r in zip(self.data, other.data):
            if l and not r:
                return False
        return True

    def from_raw(name, data):
        q = Quine2Line("-")
        q.imp = name
        q.data = [(True if elem == "X" else False) for elem in data]
        return q


class Quine2Table:
    def __init__(self, prime_imps):
        self.lines = []
        for imp in prime_imps:
            self.lines.append(Quine2Line(imp))

    def __str__(self):
        s = "Quine2Table {\n"
        for i, line in enumerate(self.lines):
            xs = "".join(("X" if b else " ") for b in line.data)
            s += f"{line.imp} {xs}\n"
        s += "}"
        return s

    def try_remove_row(self, idx):
        for i in range(len(self.lines)):
            if i == idx:
                continue
            if self.lines[idx].covered_by(self.lines[i]):
                del self.lines[idx]
                return True
        return False

    def minimize_rows(self):
        idx = 0
        while idx < len(self.lines):
            if not self.try_remove_row(idx):
                idx += 1

    def col_covers(self, idx1, idx2):
        entries_l = (line.data[idx1] for line in self.lines)
        entries_r = (line.data[idx2] for line in self.lines)
        for l, r in zip(entries_l, entries_r):
            if r and not l:
                return False
        return True

    def col_remove(self, idx):
        for line in self.lines:
            del line.data[idx]

    def try_remove_col(self, idx):
        for i in range(len(self.lines[0].data)):
            if i == idx:
                continue
            if self.col_covers(idx, i):
                self.col_remove(idx)
                return True
        return False

    def minimize_cols(self):
        idx = 0
        while idx < len(self.lines[0].data):
            if not self.try_remove_col(idx):
                idx += 1

    def minimize(self):
        while True:
            nrows, ncols = len(self.lines), len(self.lines[0].data)
            self.minimize_rows()
            self.minimize_cols()
            if (nrows, ncols) == (len(self.lines), len(self.lines[0].data)):
                break

    def from_raw(datas):
        tab = Quine2Table([])
        for name, data in datas:
            tab.lines.append(Quine2Line.from_raw(name, data))
        return tab

if __name__ == "__main__":
    #q2 = Quine2Table(["---", "0--", "00-"])
    q2 = Quine2Table.from_raw([
        ("A", "X X  X "),
        ("B", " XX X X"),
        ("C", "   XXX "),
        ("D", " XX    ")
    ])
    print(q2)
    q2.minimize()
    print(q2)
