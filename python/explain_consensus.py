from util import from_mint, to_mint, minterms_from_imp, covers, consensus_combine

class ConsensusLine:
    def __init__(self, parents, imp):
        self.parents = parents # can be none for initial terms
        self.imp = imp
        self.crossed = None # holds number of covering term if this line has been crossed.

    def is_crossed(self):
        return self.crossed is not None

    def is_active(self):
        return self.crossed is None

class ConsensusTable:
    def __init__(self):
        self.lines = []

    def add(self, parents, imp, explain=lambda s: None):
        current_idx = len(self.lines)
        new_line = ConsensusLine(parents, imp)
        for i, line in enumerate(self.lines):
            if line.is_active() and covers(line.imp, new_line.imp): # first: remove new line if already covered.
                explain(f"  > already covered by {i}; cross out immediately.")
                new_line.crossed = i
                break
        else:
            explain(f"  > not covered by an earlier implicant; add to table.")
        if new_line.is_active(): #second: if not crossed yet, remove all lines this one covers
            c = []
            for i, line in enumerate(self.lines):
                if line.is_active() and covers(new_line.imp, line.imp):
                    line.crossed = current_idx
                    c.append(i)
            if c:
                explain(f"  > covered and removed: {c}")
            else:
                explain(f"  > no lines covered by this new one.")

        self.lines.append(new_line)
        explain(f"  > new table:")
        explain(str(self))

    def execute_line(self, idx, explain=lambda s: None, prompt_continue=lambda: None):
        line = self.lines[idx]
        if line.is_crossed():
            explain(f"checking line {idx}: already crossed; move on.")
            prompt_continue()
            return
        if idx == 0:
            explain(f"checking line {idx}: active.")
        for idx2 in range(idx):
            explain(f"checking line {idx}: active.")
            line2 = self.lines[idx2]
            if line2.is_crossed():
                explain(f"> checking line {idx2}: already crossed; move on.")
                prompt_continue()
                continue
            explain(f"> checking line {idx2}: active.")
            explain(f"  > compare {idx} and {idx2}")
            explain(f"    {line.imp}")
            explain(f"    {line2.imp}")
            match = "".join(("X" if a in "01" and b in "01" and a != b else " ") for a, b in zip(line.imp, line2.imp))
            combined = consensus_combine(line.imp, line2.imp)
            if combined is not None:
                explain(f"    {match}: there is exactly 1 01/10-transition: combination possible:")
                explain(f"    {combined}")
                self.add((idx, idx2), combined, explain)
                if line.is_crossed():
                    prompt_continue()
                    explain(f"crossed current line {idx}; skipping to next one.")
                    break
            else:
                explain(f"    {match}: there are {match.count('X')} 01/10-transitions, but we need exactly 1. combination not possible.")
            prompt_continue()
        else:
            # we didn't cross out the current line and checked all above.
            explain(f"no unchecked lines left above {idx}; continue.")
        prompt_continue()

    def execute(self, explain=lambda s: None, prompt_continue=lambda: None):
        next_bottom = 0
        while next_bottom < len(self.lines): # no for-loop because table can grow
            self.execute_line(next_bottom, explain, prompt_continue)
            next_bottom += 1
        explain("no unchecked lines left; done.")
        prompt_continue()

    def unmarked(self):
        for line in self.lines:
            if line.is_active():
                yield line.imp

    def __str__(self):
        s = "ConsensusTable {\n"
        for i, line in enumerate(self.lines):
            if line.parents is not None:
                s += f"{i: >2} {line.parents[0]: >2},{line.parents[1]: >2} {line.imp}"
            else:
                s += f"{i: >2}       {line.imp}"
            if line.is_crossed():
                s += f" ({line.crossed})"
            s += "\n"
        s += "}"
        return s

def consensus(imps, interactive=False):
    if interactive:
        explain = print
        prompt_continue = lambda: input("\n\n--- (press Enter to continue) ---")
    else:
        explain = lambda s: None
        prompt_continue = lambda: None

    tab = ConsensusTable()
    for imp in imps:
        tab.add(None, imp) # don't explain these
    explain("starting table:")
    explain(tab)
    prompt_continue()
    tab.execute(explain, prompt_continue)

    explain(tab)
    prime_impls = set(tab.unmarked())
    explain(f"solution: {prime_impls}")
    return prime_impls


if __name__ == "__main__":
    terms = [
        "-010",
        "100-",
        "11-0",
        "0-01",
        "0100",
        "0111"
    ]
    res = consensus(terms, interactive=True)
    print(res)
