from util import from_mint, to_mint, weight, cost, minterms_from_imp, qmc_combine


class QmcOrderTable:
    """A single order of Quine-McCluskey table.
    """

    def __init__(self, order):
        self.table = dict() # key: weight; value: set of implicants
        self.order = order
        self.lowest_weight = 0
        self.highest_weight = 0
        self.marked = set()

    def is_empty(self):
        return len(self.table) == 0

    def add(self, imp):
        w = weight(imp)
        if w not in self.table:
            self.table[w] = set()
        self.table[w].add(imp)
        self.lowest_weight = min(self.lowest_weight, w)
        self.highest_weight = max(self.highest_weight, w)

    def unmarked(self):
        """Iterate over all unmarked implicants (prime implicants) in this table.
        """
        for ls in self.table.values():
            for imp in ls:
                if imp not in self.marked:
                    yield imp

    def __str__(self):
        s = f"QmcOrderTable (order {self.order}) {{\n"
        for w in range(self.lowest_weight, self.highest_weight + 1):
            if w in self.table:
                for imp in self.table[w]:
                    s += f"  {imp}"
                    if imp in self.marked:
                        s += f"  (x)"
                    s += "\n"
                if w != self.highest_weight:
                    s += "--------------------\n"
        s += "}"
        return s

class QmcTables:
    """Collection of several orders of Quine-McCluskey tables.
    """

    def __init__(self, table0):
        self.tables = [table0]

    def next_order(self, explain=lambda s: None, prompt_continue=lambda: None):
        """Calculate and return the next order of Quine-McCluskey table.

        It contains all possible combinations of implicants in the previous table (self).
        Used implicants in the previous table (self) are marked.
        """
        old = self.tables[-1]
        new = QmcOrderTable(len(self.tables))
        self.tables.append(new)

        explain(f"start table with order {new.order}")
        prompt_continue()
        for w in range(old.lowest_weight, old.highest_weight):
            if w in old.table and w + 1 in old.table:
                for i_s in old.table[w]:
                    for i_h in old.table[w + 1]:
                        explain(f"checking weight {w} and {w + 1}")
                        explain(f"> try combining: {i_s} and {i_h}")
                        explain(f"  {i_s}")
                        explain(f"  {i_h}")
                        def special(a, b):
                            if a in "01" and b in "01" and a != b:
                                return "X"
                            if a != b:
                                return "!"
                            return " "
                        match = "".join(special(a, b) for a, b in zip(i_s, i_h))
                        i_new = qmc_combine(i_s, i_h)
                        if i_new is not None:
                            explain(f"  {match}: terms identical except one 01/10-transition. combination possible:")
                            explain(f"  {i_new}")
                            new.add(i_new)
                            old.marked.add(i_s)
                            old.marked.add(i_h)
                            explain("> updated tables:")
                            explain(str(self))
                        else:
                            if match.count("X") == 0:
                                explain(f"  {match}: there is no 01/10-transition. cannot combine.")
                            elif match.count("X") > 1:
                                explain(f"  {match}: there is more than one 01/10-transition. cannot combine.")
                            else:
                                explain(f"  {match}: terms are not identical in positions marked with !. cannot combine.")
                        prompt_continue()
                explain(f"all terms in {w} and {w + 1} checked. continue.")
                prompt_continue()
        explain("entire table checked. done with this order.")
        prompt_continue()

        if new.is_empty():
            self.tables.pop(-1)
            return False
        return True

    def unmarked(self):
        for tab in self.tables:
            yield from tab.unmarked()

    def __str__(self):
        return "\n".join(str(tab) for tab in self.tables)

def qmc(num_vars, mints_and_dcs, interactive=False):
    """Applies the Quine-McCluskey algorithm to the given function.

    Returns: all prime implicants, including DC-only prime implicants.
    """
    if interactive:
        explain = print
        prompt_continue = lambda: input("\n\n--- (press Enter to continue) ---")
    else:
        explain = lambda s: None
        prompt_continue = lambda: None

    tab = QmcOrderTable(0)
    for term in mints_and_dcs:
        tab.add(from_mint(term, num_vars))
    explain("starting table:")
    explain(str(tab))
    tables = QmcTables(tab)

    while tables.next_order(explain, prompt_continue):
        pass
    explain("table for next order is empty. done.")
    prompt_continue()
    explain(tables)

    prime_impls = set(tables.unmarked())
    explain(f"solution: {prime_impls}")
    return prime_impls

if __name__ == "__main__":
    num_vars = 4
    mints = [1, 2, 4, 5, 7, 8, 9, 10, 12, 14]
    res = qmc(num_vars, mints, interactive=True)
    print(res)
