def from_mint(mint, num_vars):
    """Turn mint number into minterm string.

    Example: from_mint(4, 3) -> "101"
    """
    if mint >= (1 << num_vars):
        raise ValueError
    minterm = list(num_vars * "0")
    for nv in range(num_vars):
        if (mint & (1 << (num_vars - nv - 1))) != 0:
            minterm[nv] = "1"
    return "".join(minterm)

def to_mint(minterm):
    """Turn minterm string into mint number (inverse of from_mint).
    """
    mint = 0
    for c in minterm:
        mint = (mint << 1) + (1 if c == "1" else 0)
    return mint

def weight(imp):
    """Return the number of ones in an implicant.
    """
    return sum(1 for c in imp if c == "1")

def cost(imp):
    """Return the number of 0s and 1s in the implicant.

    This is equivalent to our "cost" metric (number of inputs into gate)
    """
    return sum(1 for c in imp if c in "01")

def minterms_from_imp(imp):
    """Iterate over all minterms covered by an implicant.

    Example: "1-0-" yields "1000", "1001", "1100", "1101"
    """
    if len(imp) == 1:
        if imp[0] in "01":
            yield imp[0]
        else:
            yield "0"
            yield "1"
    else:
        prev = minterms_from_imp(imp[1::])
        for elem in prev:
            if imp[0] in "01":
                yield imp[0] + elem
            else:
                yield "0" + elem
                yield "1" + elem

def covers(coverer, coveree):
    """Returns whether one implicant covers another.

    The following condition must hold in every position:
    - coverer 0: coveree 0
    - coverer 1: coveree 1
    - coverer -: coveree 0 or 1 or -
    """
    if len(coverer) != len(coveree):
        return False
    for l, r in zip(coverer, coveree):
        if l in "01" and l != r:
            return False
    return True

def num_01_10_transitions(imp1, imp2):
    if len(imp1) != len(imp2):
        raise ValueError
    t = lambda x, y: x in "01" and y in "01" and x != y
    return sum((1 if t(imp1[i], imp2[i]) else 0) for i in range(len(imp1)))

def qmc_combine_single(x, y):
    if x in "01" and y in "01" and x != y:
        return "-"
    elif x == y:
        return x
    else:
        raise ValueError

def qmc_combine(imp1, imp2):
    """Try to combine two implicants by Quine-McCluskey.

    Implicants have to be identical except for one 01 or 10 discrepancy.
    Returns: the new implicant, or None if not possible to combine.
    """
    if len(imp1) != len(imp2):
        return None
    if num_01_10_transitions(imp1, imp2) != 1:
        return None
    num_differ = sum((1 if imp1[i] != imp2[i] else 0) for i in range(len(imp1)))
    if num_differ != 1:
        return None
    return "".join(qmc_combine_single(x, y) for x, y in zip(imp1, imp2))

def consensus_combine_single(x, y):
    if x == y:
        return x
    elif x == "-":
        return y
    elif y == "-":
        return x
    return "-" # if one of them is 1 and the other is 0

def consensus_combine(imp1, imp2):
    """Try to combine two implicants by Consensus.

    Implicants have to have exactly one 01 or 10 discrepancy.
    Returns: the new implicant, or None if not possible to combine.
    """
    if len(imp1) != len(imp2):
        return None
    if num_01_10_transitions(imp1, imp2) != 1:
        return None
    return "".join(consensus_combine_single(x, y) for x, y in zip(imp1, imp2))
