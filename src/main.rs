use std::fmt;
use std::collections::{HashMap, HashSet};
use std::cmp;

trait BoolFn {
    fn num_vars(&self) -> usize;
    fn eval_at(&self, mint: usize) -> bool;
    fn mints(&self) -> HashSet<usize>;
    fn maxts(&self) -> HashSet<usize>;
}

struct BoolFnAsMints {
    num_vars: usize,
    mints: HashSet<usize>,
}

impl BoolFn for BoolFnAsMints {
    fn num_vars(&self) -> usize {
        self.num_vars
    }

    fn eval_at(&self, mint: usize) -> bool {
        if mint >= (1 << self.num_vars) {
            panic!("called function with {} variables and mint {}", self.num_vars, mint);
        }
        self.mints.contains(&mint)
    }

    fn mints(&self) -> HashSet<usize> {
        self.mints.clone()
    }

    fn maxts(&self) -> HashSet<usize> {
        let mut set = (0..(1 << self.num_vars)).collect();
        set = &set - &self.mints;
        set
    }
}

trait PartialBoolFn {
    fn num_vars(&self) -> usize;
    fn eval_at(&self, mint: usize) -> LitState;
    fn mints(&self) -> HashSet<usize>;
    fn maxts(&self) -> HashSet<usize>;
    fn dont_cares(&self) -> HashSet<usize>;
}

struct PartialBoolFnAsMintsMaxts {
    num_vars: usize,
    mints: HashSet<usize>,
    maxts: HashSet<usize>,
}

impl PartialBoolFn for PartialBoolFnAsMintsMaxts {
    fn num_vars(&self) -> usize {
        self.num_vars
    }

    fn eval_at(&self, mint: usize) -> LitState {
        if mint >= (1 << self.num_vars) {
            panic!("called function with {} variables and mint {}", self.num_vars, mint);
        }
        if self.mints.contains(&mint) {
            LitState::T
        } else if self.maxts.contains(&mint) {
            LitState::F
        } else {
            LitState::DC
        }
    }

    fn mints(&self) -> HashSet<usize> {
        self.mints.clone()
    }

    fn maxts(&self) -> HashSet<usize> {
        self.maxts.clone()
    }

    fn dont_cares(&self) -> HashSet<usize> {
        let mut set = (0..(1 << self.num_vars)).collect();
        set = &set - &self.mints;
        set = &set - &self.maxts;
        set
    }
}

impl<T: BoolFn> PartialBoolFn for T {
    fn num_vars(&self) -> usize {
        BoolFn::num_vars(self)
    }

    fn eval_at(&self, mint: usize) -> LitState {
        match BoolFn::eval_at(self, mint) {
            true => LitState::T,
            false => LitState::F,
        }
    }

    fn mints(&self) -> HashSet<usize> {
        BoolFn::mints(self)
    }

    fn maxts(&self) -> HashSet<usize> {
        BoolFn::maxts(self)
    }

    fn dont_cares(&self) -> HashSet<usize> {
        HashSet::new()
    }
}

struct Minterm {
    lits: Vec<bool>
}

impl Minterm {
    fn to_mint(&self) -> usize {
        let mut res = 0usize;
        for b in self.lits.iter().copied() {
            res = (res << 1) + if b { 1 } else { 0 };
        }
        res
    }

    fn from_mint(mint: usize, num_vars: usize) -> Result<Self, ()> {
        if mint >= (1 << num_vars) {
            return Err(());
        }
        let mut lits = vec![false; num_vars];
        for nv in 0..num_vars {
            if (mint & (1 << (num_vars - nv - 1))) != 0 {
                lits[nv] = true;
            }
        }
        Ok(Self { lits })
    }

    fn from_vec(lits: Vec<bool>) -> Self {
        Self { lits }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
enum LitState {
    F,
    T,
    DC,
}

impl LitState {
    fn from_bool(b: bool) -> Self {
        if b { Self::T } else { Self::F }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct Implicant {
    lits: Vec<LitState>,
}

impl fmt::Display for Implicant {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for lit in &self.lits {
            let c = match lit {
                LitState::F => '0',
                LitState::T => '1',
                LitState::DC => '-',
            };
            write!(f, "{}", c)?;
        }
        Ok(())
    }
}

impl Implicant {
    fn weight(&self) -> usize {
        self.lits.iter().map(|ls| match ls {
            LitState::T => 1,
            LitState::F | LitState::DC => 0,
        }).sum()
    }

    fn from_str(s: &str) -> Result<Self, ()> {
        let lits = s.as_bytes().iter().map(|b| match b {
            b'0' => Ok(LitState::F),
            b'1' => Ok(LitState::T),
            b'-' => Ok(LitState::DC),
            _ => Err(()),
        }).collect::<Result<Vec<LitState>, ()>>()?;
        Ok(Self { lits })
    }

    fn from_mint(mint: usize, num_vars: usize) -> Result<Self, ()> {
        Ok(Self::from_minterm(Minterm::from_mint(mint, num_vars)?))
    }

    fn from_minterm(m: Minterm) -> Self {
        let lits = m.lits.into_iter().map(|b| if b { LitState::T } else { LitState::F }).collect();
        Self { lits }
    }

    fn qmc_combine(&self, other: &Self) -> Option<Self> {
        if self.lits.len() != other.lits.len() {
            return None;
        }
        // TODO use result collector here?
        let mut dif_found = false;
        let mut combined = self.clone();
        for (i, (l, r)) in self.lits.iter().copied().zip(other.lits.iter().copied()).enumerate() {
            if l != r {
                if l == LitState::DC || r == LitState::DC || dif_found {
                    return None;
                }
                dif_found = true;
                combined.lits[i] = LitState::DC;
            }
        }
        dif_found.then_some(combined)
    }

    fn consensus_combine(&self, other: &Self) -> Option<Self> {
        if self.lits.len() != other.lits.len() {
            return None;
        }
        let mut dif_found = 0usize;
        let mut lits = self.lits.iter().copied().zip(other.lits.iter().copied()).map(|(l, r)| {
            if l == LitState::DC {
                r
            } else if r == LitState::DC {
                l
            } else if l == r {
                l
            } else {
                dif_found += 1;
                LitState::DC
            }
        }).collect();
        (dif_found == 1).then_some(Self { lits })
    }

    fn num_dont_cares(&self) -> usize {
        self.lits.iter().filter(|lit| **lit == LitState::DC).count()
    }

    fn len(&self) -> usize {
        self.lits.len()
    }

    // aka "bigger than", "superset"
    fn implied_by(&self, other: &Self) -> bool {
        if self.lits.len() != other.lits.len() {
            return false;
        }
        for (l, r) in self.lits.iter().copied().zip(other.lits.iter().copied()) {
            // if self has 0 or 1, the other must match that.
            if (l == LitState::T || l == LitState::F) && l != r {
                return false;
            }
        }
        return true;
    }

    fn minterms(&self) -> MintermIter<'_> {
        MintermIter {
            imp: self,
            num_dont_cares: self.num_dont_cares(),
            next_num: 0
        }
    }
}

impl cmp::PartialOrd for Implicant {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        if self.lits.len() != other.lits.len() {
            return None;
        }
        if self == other {
            return Some(cmp::Ordering::Equal);
        }
        if self.implied_by(other) {
            return Some(cmp::Ordering::Greater);
        }
        if other.implied_by(self) {
            return Some(cmp::Ordering::Less);
        }
        return None;
    }
}

struct MintermIter<'a> {
    imp: &'a Implicant,
    num_dont_cares: usize,
    next_num: usize,
}

impl<'a> Iterator for MintermIter<'a> {
    type Item = Minterm;

    fn next(&mut self) -> Option<Self::Item> {
        if self.next_num == (1 << self.num_dont_cares) {
            return None;
        }
        let mut lits = vec![false; self.imp.len()];
        let mut encountered_dont_cares = 0usize;
        for (i, lit) in self.imp.lits.iter().enumerate() {
            lits[i] = match lit {
                LitState::F => false,
                LitState::T => true,
                LitState::DC => {
                    let b = self.next_num & (1 << (self.num_dont_cares - encountered_dont_cares - 1)) != 0;
                    encountered_dont_cares += 1;
                    b
                },
            }
        }
        self.next_num += 1;
        Some(Minterm::from_vec(lits))
    }
}

/// Quine-McCluskey Table for a single order of implicants.
#[derive(Debug)]
struct QmcTable {
    /// maps from weight to implicants
    table: HashMap<usize, HashSet<Implicant>>,
    lowest_weight: usize,
    highest_weight: usize,
    marked: HashSet<Implicant>,
}

impl fmt::Display for QmcTable {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Table {{\n")?;
        for weight in self.lowest_weight..=self.highest_weight {
            if let Some(impls) = self.table.get(&weight) {
                for imp in impls {
                    write!(f, "  {}", imp)?;
                    if self.marked.contains(imp) {
                        write!(f, " (x)")?;
                    }
                    write!(f, "\n")?;
                }
                if weight != self.highest_weight {
                    write!(f, "----------------\n")?;
                }
            }
        }
        write!(f, "}}")?;
        Ok(())
    }
}

impl QmcTable {
    /// assumes the implicant is not present yet.
    fn insert(&mut self, imp: Implicant) {
        let weight = imp.weight();
        self.table.entry(weight).or_insert(HashSet::new())
            .insert(imp);
        self.lowest_weight = cmp::min(self.lowest_weight, weight);
        self.highest_weight = cmp::max(self.highest_weight, weight);
    }
    
    fn new() -> Self {
        Self { table: HashMap::new(), lowest_weight: 0, highest_weight: 0, marked: HashSet::new() }
    }

    fn next_order(&mut self) -> Self {
        let mut next = QmcTable::new();

        for weight in self.lowest_weight..self.highest_weight {
            // there may be empty levels.
            if let (Some(s_impls), Some(h_impls)) = (self.table.get(&weight), self.table.get(&(weight + 1))) {
                for i_s in s_impls.iter() {
                    for i_h in h_impls.iter() {
                        if let Some(i_new) = i_s.qmc_combine(i_h) {
                            next.insert(i_new);
                            self.marked.insert(i_s.clone());
                            self.marked.insert(i_h.clone());
                        }
                    }
                }
            }
        }
        next
    }

    fn is_empty(&self) -> bool {
        return self.table.len() == 0
    }

    fn unmarked(&self) -> impl Iterator<Item=&Implicant> {
        self.table.values().flatten()
            .filter(|imp| !self.marked.contains(imp))
    }
}

// this may also return DC-only prime implicants.
fn qmc1<P: PartialBoolFn>(p: P) -> HashSet<Implicant> {
    let mints = p.mints();
    let dcs = p.dont_cares();

    let mut tab = QmcTable::new();
    for mint in &mints {
        tab.insert(Implicant::from_mint(*mint, p.num_vars()).unwrap());
    }
    for dc in &dcs {
        tab.insert(Implicant::from_mint(*dc, p.num_vars()).unwrap());
        // we filter don't-cares later.
    }
    let mut tables = Vec::new();

    loop {
        let next = tab.next_order();
        tables.push(tab);
        if next.is_empty() {
            break;
        }
        tab = next;
    }

    let mut prime_impls: HashSet<Implicant> = HashSet::new();

    for tab in tables {
        println!("{}", tab);
        for un in tab.unmarked().cloned() {
            prime_impls.insert(un);
        }
    }

    prime_impls
}

enum CrossKind {
    /// old term is cut because a strictly bigger term is created out of it
    OldCutDirect(usize),
    /// old term is cut because another strictly bigger term was created, but not out of it
    OldCutIndirect(usize),
    /// new term is cut because it's the same or smaller than one of its parents
    NewCutDirect(usize), // this should never happen!
    /// new term is cut because it's the same or smaller than another unrelated term
    NewCutIndirect(usize)
}

struct ConsensusLine {
    // the "name" is the implicit index.
    parents: (usize, usize),
    imp: Implicant,
    crossed: Option<usize>,
}

impl ConsensusLine {
    fn new(imp: Implicant) -> Self {
        // TODO
        Self { parents: (0, 0), imp, crossed: None }
    }

    fn from(parents: (usize, usize), imp: Implicant) -> Self {
        Self { parents, imp, crossed: None }
    }
}

struct ConsensusTable {
    lines: Vec<ConsensusLine>,
}

impl ConsensusTable {
    fn execute(&mut self) {
        let mut next_bottom = 0usize;
        while next_bottom < self.lines.len() {
            for next_top in 0..next_bottom {
                self.try_combine(next_bottom, next_top);
            }
            next_bottom += 1;
        }
    }

    fn try_combine(&mut self, idx1: usize, idx2: usize) {
        if self.lines[idx1].crossed.is_some() || self.lines[idx2].crossed.is_some() {
            return;
        }
        let current_idx = self.lines.len();
        if let Some(new_imp) = self.lines[idx1].imp.consensus_combine(&self.lines[idx2].imp) {
            let mut new_line = ConsensusLine::from((idx1, idx2), new_imp);
            // first: remove new line if already covered.
            for (i, line) in self.lines.iter_mut().filter(|line| line.crossed.is_none()).enumerate() {
                if line.imp.implied_by(&new_line.imp) {
                    new_line.crossed = Some(i);
                    break;
                }
            }
            if new_line.crossed.is_none() {
                // second: if not crossed yet, remove all lines this one covers.
                // that are also not crossed yet.
                for line in self.lines.iter_mut().filter(|line| line.crossed.is_none()) {
                    if new_line.imp.implied_by(&line.imp) {
                        line.crossed = Some(current_idx);
                    }
                }
            }
            self.lines.push(new_line);
        }
    }

    fn print(&self) {
        println!("ConsensusTable {{");
        for (i, line) in self.lines.iter().enumerate() {
            print!("{:>2} {:>2},{:>2} {}", i, line.parents.0, line.parents.1, line.imp);
            if let Some(c_idx) = line.crossed {
                print!(" ({})", c_idx);
            }
            println!();
        }
        println!("}}");
    }
}

fn imp_to_qmc2line<I: Iterator<Item=usize>>(num_vars: usize, mints: I) -> Vec<bool> {
    let mut vec = vec![false; 1 << num_vars];
    for mint in mints {
        vec[mint] = true;
    }
    vec
}

fn main() {
    let mints: HashSet<usize> = [2, 5, 6, 10, 12, 13, 14, 18].iter().copied().collect();
    let maxts: HashSet<usize> = [1, 3, 8, 15, 16, 17, 21, 24, 27, 28, 31].iter().copied().collect();
    let mints: HashSet<usize> = [6, 14, 15, 19, 22, 23, 30, 31].iter().copied().collect();
    /*
    let pis = qmc1(PartialBoolFnAsMintsMaxts {
        num_vars: 5, mints, maxts
    });
    */
    let pis = qmc1(BoolFnAsMints { num_vars: 5, mints });

    // TODO only columns for the actual minterms; we don't need to cover the don't-cares!
    for pi in pis {
        println!("{}: ", pi);
        /*
        let line = imp_to_qmc2line(5, pi.minterms().map(|m| m.to_mint()));
        for item in line {
            print!("{}", if item { "X" } else { " " });
        }
        println!();
        */
    }

    let mut tab = ConsensusTable { lines: vec![
        ConsensusLine::new(Implicant::from_str("-010").unwrap()),
        ConsensusLine::new(Implicant::from_str("100-").unwrap()),
        ConsensusLine::new(Implicant::from_str("11-0").unwrap()),
        ConsensusLine::new(Implicant::from_str("0-01").unwrap()),
        ConsensusLine::new(Implicant::from_str("0100").unwrap()),
        ConsensusLine::new(Implicant::from_str("0111").unwrap()),
    ] }; 
    tab.execute();
    tab.print();
}
