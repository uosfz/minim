# minim - Minimization Algorithms for (Partial) Boolean Functions.

Algorithms implemented in both Rust and Python.

## Features (Rust)
- [X] Quine-McCluskey (`/src/main.rs`)
- [ ] Quine 2
- [X] Consensus (`/src/main.rs`)

## Features (Python)
- [X] Quine-McCluskey (`/python/explain_qmc.py`)
  - [X] interactive explainer (`/python/explain_qmc.py`)
- [X] Consensus (`/python/explain_consensus.py`)
  - [X] interactive explainer (`/python/explain_consensus.py`)
- [X] Quine 2 (`/python/quine2.py`)
